// 000000...111
// <-

#include <iostream>

int main() {
  // 8 == 2 на 3-та (4ти)
  // 4 == 2 на 2-ра (3ти)
  // 2 == 2 на 1-ва (2ри)
  // 1 == 2 на 0-лева (1ви)
  int M;
  int N;
  std::cin >> N >> M;
  const int z = 1 << (M - 1);
  if ((N & z) == z) {
    std::cout << "Bit #" << M <<" is 1;";
  } else {
    std::cout << "Bit #" << M << " is 0;";
  }

  return 0;
}