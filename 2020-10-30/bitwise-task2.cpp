#include <iostream>

int main() {
    int N;
    std::cin >> N;
    // N 101010..0100
    //   000...000100

    if ((N & 4) == 4) {
        std::cout << "Third is 1;";
    } else {
        std::cout << "Third is 0;";
    }
    return 0;
}