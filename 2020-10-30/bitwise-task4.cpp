#include <iostream>

int main() {
  int N;
  std::cin >> N;

  for (int i = 0; i < 32; i++) {
    const int z = (1 << i);
    if (N <= z) {
      std::cout << z << std::endl;
      break;
    }
  }
}