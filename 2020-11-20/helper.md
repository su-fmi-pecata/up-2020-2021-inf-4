Previous:
1. Array with predefined values:
```
int arr[10] = {-1, -1, -1, -1, -1, -1, -1, -1, -1, -1};
int arr[] = {-1, -1, -1, -1, -1, -1, -1, -1, -1, -1};
int arr[100] = {1, 2, 3, 4, 5, 6}; // array values?
int arr[100] = {0};
int arr[100] = {-1};
```
2. Proper names for read/print functions


Current:
1. What is char array;
```
char str[] = {'C','+','+','\0'};
char str[4] = {'C','+','+','\0'};
// '\0' == 0
char str[4] = "C++";
char str[100] = "C++";

const n = 5;
int arr[n];
for(int i = 0;  i < n; i++) {
  cout << arr[i];
}



char ch[50] = "C++" ;
cout << ch;
for(int i = 0;  ch[i]; i++) {
  cout << ch[i];
}
```



2. How to read;
    - `cin.get(arr, 3)`
    - `cin.get(arr, 3, '\n')`
    - `cin.getline(arr, 3)`
    - `cin.getline(arr, 3, ' ')`
4. How to print;
5. Remember '\0 '
    - What if there is no '\0'
