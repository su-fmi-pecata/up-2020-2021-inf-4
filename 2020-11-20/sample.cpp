#include <iostream>

using std::cout;
using std::cin;
using std::endl;

int length(const char* charArray) {
    int i = 0;
    while(charArray[i] != '\0') {
        i++;
    }
    return i;
}

int main() {
  char str1[] = {'C', '+', '+', '\0'};
  char str2[3] = {'C', '+', '+'};
  // '\0' == 0
  char str3[3] = {'C', '+', '+'};
  char str4[20] = "abssHhajik578291";
    cout << length(str1) << " " << str1 << endl;
    cout << length(str2) << " " << str2 << endl;
    cout << length(str3) << " " << str3 << endl;
    //cout << length(str4) << " " << str4 << endl;


    // cin >> str1;
    // cout << length(str1) << " " << str1 << endl;
    // cout << length(str2) << " " << str2 << endl;
    // cout << length(str3) << " " << str3 << endl;

        std::cin.getline(str1, 4);
        cout << length(str1) << " " << str1 << endl;
        cout << length(str2) << " " << str2 << endl;
        cout << length(str3) << " " << str3 << endl;
        std::cin.ignore();

        //str1[1];

        return 0;
}
