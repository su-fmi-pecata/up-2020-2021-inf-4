#include <iostream>

using std::cin;
using std::cout;
using std::endl;

int length(const char* charArray) {
  int i = 0;
  while (charArray[i] != '\0') {
    i++;
  }
  return i;
}

int numberOfDigits(const char* str) {
  int i = 0;
  int result = 0;
  while (str[i] != '\0') {
    if ('0' <= str[i] && str[i] <= '9') {
      result++;
    }
    i++;
  }
  return result;
}

bool areAllUpcase(const char* str) {
  for (int i = 0; str[i] != '\0'; i++) {
    if (!('A' <= str[i] && str[i] <= 'Z')) {
      return false;
    }
  }
  return true;
}

bool isNumber(const char c) { return '0' <= c && c <= '9'; }

bool isValidNaturalNumber(const char* str) {
  if (str[0] == '\0') {
    return false;
  }

  if (!isNumber(str[0]) || str[0] == '0') {
    return false;
  }

  for (int i = 1; str[i] != '\0'; i++) {
    if (!isNumber(str[i])) {
      return false;
    }
  }

  return true;
}

bool same(const char* a, const char* b) {
  int i = 0;
  while (a[i] && b[i] && a[i] == b[i]) {
    i++;
  }
  return a[i] == b[i];
}

int numberOf(const char symbol, const char* str) {
  int result = 0;
  for (int i = 0; str[i] != '\0'; i++) {
    if (str[i] == symbol) {
      result++;
    }
  }
  return result;
}

void printCaeser(const char* str, int x) {
  for (int i = 0; str[i] != '\0'; i++) {
    cout << ((char)(str[i] + x));
  }
}

void printFence(const char* str) {
  const int len = length(str);
  for (int i = 0; i < len; i += 2) {
    cout << str[i];
  }
  for (int i = 1; i < len; i += 2) {
    cout << str[i];
  }
}

int main() {
  printCaeser("ACDC", 1);
  return 0;
}