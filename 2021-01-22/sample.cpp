#include <iostream>

// #include "sample-color.cpp"
#include "sample-filter.cpp"
#include "sample-array.cpp"

//         sample
//       /       \
// sample-enum   sample-color
//                /
//               sample-enum

#include <functional>


void print(const int* arr, const int size) {
  if (size == 0) {
    std::cout << "Empty array\n";
    return;
  }

  for (int i = 0; i < size; i++) {
    std::cout << arr[i] << " ";
  }
  std::cout << "\n";
}

void print(const Array array) { print(array.arr, array.size); }


Array map(const int arr[], const int size,
          std::function<int(const int)> manipulate) {
  int* res = new int[size];
  int resCount = 0;
  for (int i = 0; i < size; i++) {
    res[resCount] = manipulate(arr[i]);
    resCount++;
  }

  Array array;
  array.arr = res;
  array.size = resCount;
  return array;
}

bool isPrime(const int n) { return true; }
bool isEven(const int n) { return n % 2 == 0; }
// [](const int n) { return n % 2 == 0; }

std::function<int(const int)> multiply(const int arr0) {
    return [&](const int n) { return arr0 * n; };
}

bool areSame(Array a, const int * arr, const int size) {
    if(a.size != size) {
        return false;
    }

    for(int i = 0;  i < size; i++) {
        if(a.arr[i] != arr[i]) {
            return false;
        }
    }

    return true;
}

int main() {
  const int arr[] = {5, 4, 3, 2};
  const int size = 4;


  print(arr, size);
  const Array filtered = filter(arr, size, isPrime);
  print(filtered);
  print(filter(arr, size, isEven));
  print(filter(arr, size, [](const int n) { return n % 2 == 0; }));
  print(filter(arr, size, [](const int n) { return n % 2 == 1; }));
  std::cout << "Mapping\n";
  print(map(arr, size, [](const int n) { return n + 1; }));
  print(map(arr, size, [](const int n) { return 2 * n; }));
  print(map(arr, size, [&](const int n) { return arr[0] * n; }));
  print(map(arr, size, [&](const int n) { return arr[0] * n; }));
  print(map(arr, size, multiply(arr[0])));

    std::cout << areSame(filter(arr, size, isPrime), arr, size) << "\n";

  return 0;
}

///  std::function<bool(const int)>