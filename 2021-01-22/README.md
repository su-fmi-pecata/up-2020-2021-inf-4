# Enums
```cpp
enum enum_name{const1, const2, ....... };
```

Be carefull when setting const a custom value!!! (duplicates)

Usefull when finite sets.
```cpp
enum colors { red, black, green, yellow };
enum suit { heart, diamond, spade, club };
enum classes { Programming101, Algebra, Statistics, FunctionalProgramming, OOP };
```





# Main arguments
```cpp
int main(int argc, char *argv[]) {

    return 0;
}
```

Args: 
- `argv[0]` is the command with which the program is invoked;
- `argv[1]` is the first command-line argument;
- `argv[argc - 1]` is the last commad-line argument;
- `argv[argc]` is always NULL.





# Preprocessor
```cpp
#define TABLE_SIZE 100
int table1[TABLE_SIZE];
int table2[TABLE_SIZE]; 

#define getmax(a,b) a>b?a:b

#undef TABLE_SIZE

#if defined ARRAY_SIZE
#define TABLE_SIZE ARRAY_SIZE
#elif !defined BUFFER_SIZE
#define TABLE_SIZE 128
#else
#define TABLE_SIZE BUFFER_SIZE
#endif 

#include <header>
#include "file" 
```

Look at [that](https://www.cplusplus.com/doc/tutorial/preprocessor/).






# Conditional compilation
```cpp
#ifndef _NOT_DEFINED_
#define _NOT_DEFINED_

.... code ....
/* This is a standard 
 * comment.
 */  
.... code ....

#endif
```

If you want to use it, PLEASE READ [THIS](https://stackoverflow.com/a/4473825).






# Structures
```cpp
struct structureName{
    member1;
    member2;
    member3;
    .
    .
    .
    memberN;
};

struct Point 
{ 
   int x, y; 
};

struct Point3
{ 
   int x, y, z;
};

int main() {
    Point p1;
    p1.x = 3;
    p1.y = 4;

    Point3 p2;
    p2.x = 3;
    p2.y = 4;
    p2.z = 5;
}

struct Student {
    int fn;
    char* name;
    char* egn;
    int grade;
};
```






# "Abstract" functions
```cpp
#include <functional>

int main() {
    std::function<bool(const int)> isEven = [](const int a) { return a % 2 == 0; };
    std::function<bool(const int)> isOdd = [&](const int a) { return !isEven(a); };
    std::function<bool(const int, const int)> equals = [](const int a, const int b) { return a == b; };

}
```
