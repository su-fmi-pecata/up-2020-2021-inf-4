#ifndef _INCLUDE_SAMPLE_ENUM_CPP
#define _INCLUDE_SAMPLE_ENUM_CPP

#include <iostream>
#include "sample-array.cpp"

using std::cout;

Array filter(const int arr[], const int size,
             std::function<bool(const int)> predicate) {
  int* res = new int[size];
  int resCount = 0;
  for (int i = 0; i < size; i++) {
    if (predicate(arr[i])) {
      res[resCount++] = arr[i];
    }
  }

  Array array;
  array.arr = res;
  array.size = resCount;
  return array;
}

#endif // _INCLUDE_SAMPLE_ENUM_CPP