#include <iostream>

using std::cout;
using std::cin;
using std::endl;

int getLength(const char arr[]) {
    int i = 0;
    while(arr[i]) {
        i++;
    }
    return i;
}

void readMatrix(char** m, const int n) {
    for (int r = 0;  r < n;  r++) {
        for (int c = 0;  c < n;  c++) {
            cin >> m[r][c];
        }
    }
}


// arr:  abcbdef
// word: bd
bool containsInArr(const char* arr, const char* word, const int arrayLen, const int wordLen) {
    for(int i =0;  i < arrayLen - wordLen; i++) {
        if(arr[i] == word[0]) {
            bool contains = true;
            for(int w = 0;  w < wordLen;  w++) {
                if(arr[i + w] != word[w]) {
                    contains = false;
                    break;
                }
            }
            if(contains) { 
                return true;
            }
        }
    }
    return false;
}

bool containsInDiag(const char** m, const char* word, const int arrayLen, const int wordLen) {
    char* d = new char[arrayLen];
    for(int i = 0;  i < arrayLen;  i++) {
        d[i] = m[i][i];
    }
    const bool result = containsInArr(d, word, arrayLen, wordLen);
    delete[] d;
    return result;
}

int main() {
    int n;
    cin >> n;
    const int MAX_SIZE = 10;
    char arr[MAX_SIZE][MAX_SIZE];
    readMatrix(arr, n);
    char word[MAX_SIZE];
    cin.getline(word, MAX_SIZE);
    const int wordLen = getLength(word);
    if(wordLen > n) {
        cout << "Too Long Word";
        return 1;
    }



    return 0;
}