// <type>* <name> = new <type>[<number];
// <type>* <name> = new <type>;

// int* a = new int;
// *a


// delete <name>;
// delete[] <name>;

#include <iostream>

double* copy_of(const double* arr, const int size) {
    double* result;
    result = new double[size];
    // double result[size];
    for(int i = 0;  i < size; i++) {
        result[i] = arr[i];
    }
    return result;
}

void print_array(const double arr[], const int size) {
    for(int i = 0;  i < size; i++) {
        std::cout << arr[i] << " ";
    }
    std::cout << std::endl;
}

int main() {
    const int size = 5;
    double arr[] = {1, 2, 3, 6, 7};
    double* newArr = copy_of(arr, size);
    double* newArr1 = copy_of(arr, size);
    double* newArr2 = copy_of(arr, size);
    double* newArr3 = copy_of(arr, size);
    double* newArr4 = copy_of(arr, size);
    double* newArr5 = copy_of(arr, size);
    print_array(arr, size);
    print_array(newArr, size);
    newArr[3] = 123;
    print_array(arr, size);
    print_array(newArr, size);
    delete[] newArr;
    delete[] newArr1;
    delete[] newArr2;
    delete[] newArr3;
    delete[] newArr4;
    delete[] newArr5;

    return 0;
}