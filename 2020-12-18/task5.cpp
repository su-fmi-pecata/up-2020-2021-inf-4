#include <iostream>

using std::cout;
using std::cin;
using std::endl;

int getLength(const char arr[]) {
    int i = 0;
    while(arr[i]) {
        i++;
    }
    return i;
}

char* concat_v2(const char* a, const char* b) {
    char* result = new char[getLength(a) + getLength(b) + 1];
    bool getFromA = true;
    int indexA = 0;
    int indexB = 0;
    int indexResult = 0;
    while(a[indexA] || b[indexB]) {
        if(getFromA && a[indexA]) {
            result[indexResult++] = a[indexA++];
        } else if(b[indexB]) {
            result[indexResult++] = b[indexB++];
        }
        getFromA = !getFromA;
    }
    result[indexResult] = '\0';
    return result;
}

int main() {
    const char* a = "abcdefg";
    const char* b = "poiuytrwq";
    char* c = concat_v2(a, b);
    cout << c << endl;
    delete[] c;
    return 0;
}