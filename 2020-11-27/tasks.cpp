#include <iostream>

using std::cin;
using std::cout;
using std::endl;

void print(const int arr[], const int n) {
    for(int i = 0;  i < n;  i++) {
        cout << arr[i] << " ";
    }
}

void read(int arr[], const int n) {
    for(int i = 0;  i < n;  i++) {
        cin >> arr[i];
    }
}

int readInt() {
    int n;
    cin >> n;
    return n;
}

double minElement(double arr[], const int n) {
    double min = arr[0];
    for(int i = 1;  i < n; i++) {
        if(min > arr[i]) {
            min = arr[i];
        }
    }
    return min;
}

int indexMaxElement(double arr[], const int n) {
    int maxIndex = 0;
    for(int i = 1;  i < n; i++) {
        if(arr[maxIndex] < arr[i]) {
            maxIndex = i;
        }
    }
    return maxIndex;
}

void doubleElements(int arr[], const int n) {
    for(int i = 0;  i < n;  i++) {
        // arr[i] *= 2;
        arr[i] = arr[i] * 2;
    }
}

int numberOfDigits(int n) {
    if(n == 0) {
        return 1;
    }

    int num = 0;
    while(n > 0) {
        num++;
        // n /= 10;
        n = n / 10;
    }
    return num;
}

void printFormattedElement(const int printNumber1, const int printNumber2) {
    const int len1 = numberOfDigits(printNumber1);
    const int len2 = numberOfDigits(printNumber2);

    if(len1 < len2) {
        for(int i = len1;  i < len2;  i++) {
            cout << " ";
        }
    }

    cout << printNumber1 << " ";
}

void printDoubleElements(const int arr[], const int n) {
    for(int i = 0;  i < n;  i++) {
        printFormattedElement(arr[i] * 2, arr[i]);
    }
    cout << endl;
    for(int i = 0;  i < n;  i++) {
        printFormattedElement(arr[i], arr[i] * 2);
    }
}

bool deleteElementAtIndex(int arr[], const int n, const int index) {
    if(index >= n || index < 0) {
        return false;
    }
// 2 9 5 4 3 value 2 5 7 5 4 3 2
    for(int i = index + 1; i < n; i++) {
        arr[i - 1] = arr[i];
    }
    return true;
}

bool updateElementAtIndex(double arr[], const int n,
 const int index, const double value)  {
     if(index < 0 || index >= n) {
         return false;
     }

     arr[index] = value;
     return true;
 }

bool insertElementAtIndex(int arr[], const int n,
 const int index, const int value) {

     if(index < 0 || index > n) {
         return false;
     }

     for(int i = n;  i >= index;  i--) {
         arr[i] = arr[i - 1];
     }

     arr[index] = value;
     return true;
 }

bool isEven(const int n) {
    return n % 2 == 0;
}

bool isLeapYear(const int year) { return false;}

int numberOfEven(const int arr[], const int n) {
    int num = 0;
    for(int i = 0; i < n; i++) {
        if(isEven(arr[i])) {
            num++;
        }
    }
    return num;
}

int sumOfSmallerOdd(const int n) {
    int sum = 0;
    for(int i = 1;  i < n;  i+=2) {
        sum += i;
    }
    return sum;
}

void calculateTask15(int arr[],const int n) {
    for(int i = 0;  i < n;  i+=2 ){
        int value;
        if(isEven(arr[i])) {
            value= arr[i]/2;
        } else {
            value = sumOfSmallerOdd(arr[i]);
        }
        insertElementAtIndex(arr, n, i+1, value);
    }
}


int main() {
    // const int n = readInt();
    // int arr[128];
    // read(arr, n);
    // printDoubleElements(arr, n);
    const int MAX = 5000 * 2;
    const int n = readInt();
    int arr[MAX];
    read(arr, n);
    print(arr, n);
    cout << endl;
    calculateTask15(arr, n*2);
    print(arr, n*2);
    return 0;
}