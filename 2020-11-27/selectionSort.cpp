// 9 2 5 4 3 2 5 7 5 4 3 2
// 2 9 5 4 3 2 5 7 5 4 3 2
// 2 2 5 4 3 9 5 7 5 4 3 2
// 2 2 2 4 3 9 5 7 5 4 3 5
// 2 2 2 3 4 9 5 7 5 4 3 5

#include <iostream>

using std::cin;
using std::cout;
using std::endl;

void print(const int arr[], const int n) {
    for(int i = 0;  i < n;  i++) {
        cout << arr[i] << " ";
    }
}

void selectionSort(int arr[], const int n) {
    for(int sortingToIndex = 0; sortingToIndex < n - 1;  sortingToIndex++) {
        int minIndex = sortingToIndex;
        for(int checkIndex = sortingToIndex + 1;  checkIndex < n;  checkIndex++) {
            if(arr[minIndex] > arr[checkIndex]) {
                minIndex = checkIndex;
            }
        }
        int c = arr[minIndex];
        arr[minIndex] = arr[sortingToIndex];
        arr[sortingToIndex] = c;
    }
}

int main() {
    const int MAX = 128;
    int n;
    cin >> n;
    int arr[MAX];
    for(int i = 0;  i < n;  i++) {
        cin >> arr[i];
    }
    print(arr, n);
    cout << endl;

    selectionSort(arr, n);

    print(arr, n);
    cout << endl;

    return 0;
}