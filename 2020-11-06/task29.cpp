#include <iostream>

using std::cout;
using std::endl;

unsigned int findNumberOfDigits(unsigned int n) {
  if (n == 0) {
    return 1;
  }

  int count = 0;

  while (n > 0) {
    n /= 10;
    count++;
  }

  return count;
}

// a^b
int power(const int a, const int b) {
  int result = 1;
  for (int i = 0; i < b; i++) {
    result = result * a;
  }
  return result;
}

bool isMunhausen(const unsigned int n) {
  const unsigned int numberOfDigits = findNumberOfDigits(n);
  int nBackup = n;
  int result = 0;
  for (int i = 0; i < numberOfDigits; i++) {
    const int lastDigit = nBackup % 10;
    result += power(lastDigit, lastDigit);
    nBackup /= 10;
  }
  return n == result;
}

int main() {
  cout << isMunhausen(1) << endl;
  cout << isMunhausen(2) << endl;
  cout << isMunhausen(5) << endl;
  cout << isMunhausen(7) << endl;
  cout << isMunhausen(3435) << endl;
  cout << isMunhausen(7989) << endl;
  return 0;
}