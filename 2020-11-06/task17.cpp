#include <iostream>

using std::cout;
using std::endl;

bool isPrime(const unsigned int n) {
  if (n < 2) {
    return false;
  }

  for (int i = 2; i < n; i++) {
    if (n % i == 0) {
      return false;
    }
  }

  return true;
}

int cube(int n) { return n * n * n; }

bool isCubePrime(const unsigned int n) {
  if (!isPrime(n)) {
    return false;
  }

  // (n+1)^3 - n^3 = 3n*2 + 3n + 1 > n
  for (int i = 1; i < n; i++) {
    if (n == cube(i + 1) - cube(i)) {
      return true;
    }
  }

  return false;
}

int main() {
  cout << isCubePrime(1) << endl;
  cout << isCubePrime(7) << endl;
  cout << isCubePrime(8) << endl;
  cout << isCubePrime(10) << endl;
  cout << isCubePrime(61) << endl;
  return 0;
}