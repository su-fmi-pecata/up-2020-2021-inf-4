# 1D Arrays
- What is an array
- Why do we need arrays
- How to PROPERLY create one
    + use const or macro
- How to use arrays
- Arrays as function arguments
    + use const `void fn(const int arr[], const int n)`
- `arr[arr[z]]`

