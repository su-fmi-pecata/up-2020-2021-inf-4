#include <iostream>

using std::cin;
using std::cout;
using std::endl;

// 1 2 3 4 5  for(int i = 1;  i <= 5; i++)
// 0 1 2 3 4  for(int i = 0;  i < 5;  i++)

const int numberOfStudents = 5;

void readArray(double arr[], const int n) {
  for (int i = 0; i < n; i++) {
    cin >> arr[i];  // n(i+1)
  }
}

void printArray(const double arr[], const int n) {
  for (int i = 0; i < n; i++) {
    cout << arr[i] << endl;  // n(i+1)
  }
}

int main() {
    // <type> <name>[<number_of_elements];
    double students[numberOfStudents];
    // students[0]
    // NUMBER_OF_ELEMENTS//
    readArray(students, numberOfStudents);
    printArray(students, numberOfStudents);

    const int z = 4;
    int intArr[z];
    intArr[0] = 3;
    intArr[1] = 4;
    intArr[2] = 5;
    intArr[3] = 6;
    const int k = 7;
    intArr[intArr[k]]; // == 6
        // cin >> students[1]; // n2
        // cin >> students[2]; // n3

    return 0;
}