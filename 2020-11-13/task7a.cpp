#include <iostream>

using std::cin;
using std::cout;
using std::endl;

const int MAX_ARRAY_SIZE = 128;

int readInt() {
  int k;
  cin >> k;
  return k;
}

void readArray(int arr[], const int arraySize) {
  for (int i = 0; i < arraySize; i++) {
    cin >> arr[i];  // n(i+1)
  }
}

void shuffle(int arr[], const int arraySize) {
    for(int i = 0;  i < arraySize / 2;  i++) {
        if(i%2 == 1) {
          const int index2 = (arraySize - 1) - i;
           const int c = arr[i];
          arr[i] = arr[index2];
          arr[index2] = c;
        }
    }
}

void printArray(const int arr[], const int arraySize) {
  for (int i = 0; i < arraySize; i++) {
    cout << arr[i] << " ";  // n(i+1)
  }
}

int main() {
    const int N = readInt();
    int arr[MAX_ARRAY_SIZE];
    readArray(arr, N);
    
    cout << "Read  : ";
    printArray(arr, N);
    
    shuffle(arr, N);

    cout << endl << "Result: ";
    printArray(arr, N);
    
    return 0;
}