#include <iostream>

using std::cin;
using std::cout;
using std::endl;

const int MAX_ARRAY_SIZE = 128;

int read() {
  int k;
  cin >> k;
  return k;
}

void readArray(double arr[], const int arraySize) {
  for (int i = 0; i < arraySize; i++) {
    cin >> arr[i];  // n(i+1)
  }
}

double minInArray(const double arr[], const int arraySize) {
  double min = arr[0];
  for(int i = 1;  i < arraySize;  i++) {
      if(min > arr[i]) {
          min = arr[i];
      }
  }
  return min;
}

int main() {
  const int N = read();
  if (N > 100) {
    cout << "Invalid input";
    return 1;
  }

  double arr[MAX_ARRAY_SIZE];
  readArray(arr, N);
  cout << "MIN is " << minInArray(arr, N);
  return 0;
}
