#include <iostream>

using std::cin;
using std::cout;
using std::endl;

const int MAX_ARRAY_SIZE = 128;

int readInt() {
  int k;
  cin >> k;
  return k;
}

void readArray(int arr[], const int arraySize) {
  for (int i = 0; i < arraySize; i++) {
    cin >> arr[i];  // n(i+1)
  }
}

// ... p-2 p-1 p   p+1 p+2 ... arr
// ... result[p-2] = arr[p-2] + arr[p-1]
// ... result[p-1] = arr[p-1] + arr[p]

void generateResult(const int arr[], int result[], const int arrSize) {
  // arrSize - 1 == i
  // arr[i] + arr[i+1] == arr[arrSize - 1] + arr[arrSize]
  for(int i = 0;  i < arrSize -1; i++)  {
      result[i] = arr[i] + arr[i+1];
  }
}

int maxInArray(const int arr[], const int arraySize) {
  int max = arr[0];
  for (int i = 1; i < arraySize; i++) {
    if (max < arr[i]) {
      max = arr[i];
    }
  }
  return max;
}

int main() {
    const int N = readInt();
    int arr[MAX_ARRAY_SIZE];

    readArray(arr, N);

    const int N_RES = N - 1;
    int result[MAX_ARRAY_SIZE];

    generateResult(arr, result, N);
    cout << "Result: " << maxInArray(result, N_RES);
    return 0;
}