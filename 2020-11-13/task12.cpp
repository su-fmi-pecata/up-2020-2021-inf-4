#include <iostream>

using std::cin;
using std::cout;
using std::endl;

const int MAX_ARRAY_SIZE = 128;

int readInt() {
  int k;
  cin >> k;
  return k;
}

void readArray(int arr[], const int arraySize) {
  for (int i = 0; i < arraySize; i++) {
    cin >> arr[i];  // n(i+1)
  }
}

// ... p-2 p-1 p   p+1 p+2 ... arr
// ... result[p-2] = arr[p-3] + arr[p-1]
// ... result[p-1] = arr[p-2] + arr[p]

void generateResult(const int arr[], int result[], const int arrSize) {
  for (int i = 1; i < arrSize - 1; i++) {
    result[i] = arr[i - 1] + arr[i + 1];
  }
}

int getIndexOfMaxInArray(const int arr[], const int arraySize, const int startFromIndex) {
  int maxIndex = startFromIndex;
  for (int i = startFromIndex + 1; i < arraySize; i++) {
    if (arr[maxIndex] < arr[i]) {
      maxIndex = i;
    }
  }
  return maxIndex;
}

int main() {
  const int N = readInt();
  int arr[MAX_ARRAY_SIZE];

  readArray(arr, N);

  const int N_RES = N - 1;
  int result[MAX_ARRAY_SIZE];

  generateResult(arr, result, N);
  cout << "Result: " << getIndexOfMaxInArray(result, N_RES, 1);
  return 0;
}