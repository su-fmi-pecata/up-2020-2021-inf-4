#include <iostream>

using std::cin;
using std::cout;
using std::endl;

const int MAX_ARRAY_SIZE = 128;

int readInt() {
  int k;
  cin >> k;
  return k;
}

void readArray(int arr[], const int arraySize) {
  for (int i = 0; i < arraySize; i++) {
    cin >> arr[i];  // n(i+1)
  }
}

void printNumbers(const int arr[], const int arraySize) {
  for (int i = 0; i < arraySize; i++) {
    if(arr[i] % 2 == 0) {
        cout << arr[i] << " ";
    }
  }
}

int main() {
  const int N = readInt();
  int arr[MAX_ARRAY_SIZE];

  readArray(arr, N);
  printNumbers(arr, N);
  return 0;
}