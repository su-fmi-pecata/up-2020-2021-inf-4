#include <iostream>

using std::cin;
using std::cout;
using std::endl;

const int MAX_ARRAY_SIZE = 128;

int read() {
    int k;
    cin >> k;
    return k;
}

void readArray(int arr[], const int arraySize) {
  for (int i = 0; i < arraySize; i++) {
    cin >> arr[i];  // n(i+1)
  }
}

void printArray(const int arr[], const int arraySize) {
  for (int i = 0; i < arraySize; i++) {
    cout << arr[i] << endl;  // n(i+1)
  }
}

int main() {
    const int N = read();
    if(N > 100) {
        cout << "Invalid input";
        return 1;
    }

    int arr[MAX_ARRAY_SIZE];
    readArray(arr, N);
    printArray(arr, N);
    return 0;
}