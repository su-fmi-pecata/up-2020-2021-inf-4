#include<iostream>
#include<cmath>
#include<cstring>
using std::cin;
using std::cout;
using std::endl;

const int MAX_COLS = 10;

void read_matrix(int matrix[][MAX_COLS], int rows, int cols) {
    for(int i = 0; i < rows; i++) {
        for(int j = 0; j < cols; j++) {
            cin >> matrix[i][j];
        }
    }
}

void read_matrix(char matrix[][MAX_COLS], int rows, int cols) {
    for(int i = 0; i < rows; i++) {
        for(int j = 0; j < cols; j++) {
            cin >> matrix[i][j];
        }
    }
}

void print_matrix(int matrix[][MAX_COLS], int rows, int cols) {
    for(int i = 0; i < rows; i++) {
        for(int j = 0; j < cols; j++) {
            cout << matrix[i][j] << ' ';
        }
        cout << endl;
    }
}

void print_matrix(bool matrix[][MAX_COLS], int rows, int cols) {
    for(int i = 0; i < rows; i++) {
        for(int j = 0; j < cols; j++) {
            cout << matrix[i][j] << ' ';
        }
        cout << endl;
    }
}

void find_divisible_by_3(int matrix[][MAX_COLS], int rows, int cols, bool result[][MAX_COLS]) {
    for(int i = 0; i < rows; i++) {
        for(int j = 0; j < cols; j++) {
            result[i][j] = (matrix[i][j] % 3 == 0);
        }
    }
}

void multiply(int a[][MAX_COLS], int b[][MAX_COLS], int size, int result[][MAX_COLS]) {
    for(int i = 0; i < size; i++) {
        for(int j = 0; j < size; j++) {
            result[i][j] = 0;
            for(int k = 0; k < size; k++) {
                result[i][j] += a[i][k] * b[k][j];
            }
        }
    }
}

void to_array(int matrix[][MAX_COLS], int size, int arr[]) {
    int index_in_arr = 0;

    for(int i = 0; i < size; i++) {
        for(int j = 0; j < size; j++) {
            arr[index_in_arr] = matrix[i][j];
            index_in_arr++;
        }
    }
}

void to_matrix(int arr[], int size_of_arr, int matrix[][MAX_COLS]) {
    int size_of_matrix = sqrt(size_of_arr);
    int index_in_arr = 0;

    for(int i = 0; i < size_of_matrix; i++) {
        for(int j = 0; j < size_of_matrix; j++) {
            matrix[i][j] = arr[index_in_arr];
            index_in_arr++;
        }
    }
}

// Selection sort
void sort_matrix(int matrix[][MAX_COLS], int rows, int cols) {

    int number_of_elements = rows * cols;

    for(int i = 0; i < number_of_elements - 1; i++) {
        for(int k = i+1; k < number_of_elements; k++) {
            
            int row_i = i / cols;
            int col_i = i % cols;

            int row_k = k / cols;
            int col_k = k % cols;
            
            if(matrix[row_k][col_k] < matrix[row_i][col_i]) {
                int swap = matrix[row_i][col_i];
                matrix[row_i][col_i] = matrix[row_k][col_k];
                matrix[row_k][col_k] = swap;
            }
        }
    }
}

void print_strings_with_six_chars(char words[][20]) {
    for(int i = 0; i < 8; i++) {
        if(strlen(words[i]) == 6) {
            cout << words[i];
        }
    }
}

void copy_string(char clone[], char original[]) {
    int i = 0;
    while(original[i] != '\0') {
        clone[i] = original[i];
        i++;
    }
    clone[i] = '\0';
}

void sort_words_by_length(char words[][20]) {

    int lengths[8];

    for(int i = 0; i < 8; i++) {
        lengths[i] = strlen(words[i]);
    }

    for(int i = 0; i < 7; i++) {
        for(int k = i+1; k < 8; k++) {

            if(lengths[k] < lengths[i]) {
                char swap[20];
                copy_string(swap, words[i]);
                copy_string(words[i], words[k]);
                copy_string(words[k], swap);
            }
        }
    }
}

// coffee < pizza
// pizza > pie
// pie < pizza
// Java < JavaScript

// strcmp("pizza", "pie") > 0
// strcmp("pie", "pizza") < 0
// strcmp("up", "up") == 0

void sort_words_lex(char words[][20]) {
    for(int i = 0; i < 7; i++) {
        for(int k = i+1; k < 8; k++) {

            if(strcmp(words[k], words[i]) < 0) {    // < 0, ако левият низ е "по-малък" от десния
                                                    // = 0, ако двата са еднакви
                                                    // > 0, ако левият низ е "по-голям" от десния
                char swap[20];
                copy_string(swap, words[i]);
                copy_string(words[i], words[k]);
                copy_string(words[k], swap);
            }
        }
    }
}

int count_occurrences(char matrix[][MAX_COLS], int rows, int cols, const char word[]) {
    int count = 0;

    for(int i = 0; i < rows; i++) {
        for(int k = 0; k < cols; k++) {
            if(matrix[i][k] == word[0]) {

                int p = 0;

                while(k + p < cols && word[p] != '\0' && matrix[i][k + p] == word[p]) {
                    p++;
                }

                if(word[p] == '\0') {
                    count++;
                }
            }
        }
    }

    return count;
}

int main() {
    // int matrix[MAX_COLS][MAX_COLS];
    // int rows, cols;

    // cout << "Enter number of rows and cols: ";
    // cin >> rows >> cols;

    // read_matrix(matrix, rows, cols);

    // sort_matrix(matrix, rows, cols);

    // print_matrix(matrix, rows, cols);

    char matrix[MAX_COLS][MAX_COLS];
    int rows, cols;
    cout << "Enter number of rows and cols: ";
    cin >> rows >> cols;

    read_matrix(matrix, rows, cols);

    cout << count_occurrences(matrix, rows, cols, "c++") << endl;
}