#include <iostream>

using std::cout;
using std::endl;

// 0 1 1 2 3 5 8 .....
// f[0] = 0
// f[1] = 1
// f[n] = f[n-1] + f[n-2]

unsigned int cache[100] = {0};

unsigned int fib(const int n)  {
    if(n == 0) {
        return 0;
    }
    if(n == 1) {
        return 1;
    }
    return fib(n-1) + fib(n-2);
}

unsigned int fibCache(const int n)  {
    if(n == 0) {
        return 0;
    }
    if(n == 1) {
        return 1;
    }
    if(cache[n-1] == 0) {
        cache[n-1] = fibCache(n-1);
    }
    if(cache[n-2] == 0) {
        cache[n-2] = fibCache(n-2);
    }
    return cache[n-1] + cache[n-2];
}

unsigned int fibIter(const int n) {
    if(n == 0) {
        return 0;
    }
    if(n == 1) {
        return 1;
    }
    unsigned int curr = 1;
    unsigned int prev = 0;

    for(int i = 1;  i < n;  i++) {
        unsigned int next = curr + prev;
        prev = curr;
        curr = next;
    }

    return curr;
}


// fib(5) == fib(4) + fib(3) // 3 + 2
// fib(4) == fib(3) + fib(2) // 2 + 1
// fib(3) == fib(2) + fib(1) // 2
// fib(2) == fib(1) + fib(0) // 1
//


int main() {
    cout << fibCache(5) << endl;
    cout << fibIter(5) << endl;
    cout << fibCache(10) << endl;
    cout << fibIter(10) << endl;
    cout << fibCache(15) << endl;
    cout << fibIter(15) << endl;
    cout << fibCache(20) << endl;
    cout << fibIter(20) << endl;
    // cout << fibCache(25) << endl;
    // cout << fibCache(50) << endl;
    // cout << fibCache(15) << endl;
    // cout << fibCache(20) << endl;
    // cout << fibCache(25) << endl;
    // cout << fibCache(50) << endl;
    return 0;
}