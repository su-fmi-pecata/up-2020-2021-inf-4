// n! = 1*2*...*n
// n! = n*(n-1)!
// 0! = 1
// not exists (-1)!

// дъно
// стъпка -> Промяна


#include <iostream>
using std::cout;
using std::endl;



unsigned int fact(const int n) {
    if(n < 0) {
        return 0;
    }

    unsigned int result = 1;
    for(int i = n;  i > 1;  i--) {
        result *= i;
    }
    return result;
}

unsigned int factRecursion(const int n) {
    if(n < 0) {
        return 0;
    }
    if(n == 0) {
        return 1;
    }
    return n*factRecursion(n-1);
}

void printDigitsHelper(const int n, const int toBePrinted) {
    if(n == 0) {
        cout << '0';
        return;
    }
    if(toBePrinted == 0) {
        return;
    }
    cout << (n, toBePrinted % 10) << " ";
    printDigitsHelper(n, toBePrinted / 10);
}

void printDigits(const int n){
   printDigitsHelper(n, n);
}

// find a way to proper handle n < 0
void printDigitsV2(const int n) {
    if(n < -9) {
        cout << "-";
        printDigitsV2(-n);
        return;
    }
    if(n < 10 && n >= 0) {
        cout << n;
        return;
    }
    cout << (n % 10) << " ";
    printDigitsV2(n / 10);
}

void printDigitsVariantA(const int n) {
    if(n == 0) {
        return;
    }
    const int last = n % 10;
    printDigitsVariantA(n / 10);
    cout << last << " ";
}

void printString(const char* str) {
    if(*str == '\0') {
        return;
    }
    cout << *str;
    printString(str + 1);
}

void printSeveral(const char sym, const int n) {
    if(n <= 0) {
        return;
    }

    cout << sym;
    printSeveral(sym, n-1);
}

void printLetters(const char start, const char end) {
    if(start > end) {
        return;
    }

    cout << start;
    printLetters(start+1, end);
}

unsigned int count1(const int n) {
    if( n == 0) {
        return 0;
    }

    if(n % 2 == 0) {
        return count1(n/2);
    }

    return 1 + count1(n/2);
} 

int main() {
    cout << count1(5) << endl; // 101
    cout << count1(1) << endl; // 001
    cout << count1(0) << endl; // 000
    cout << count1(2) << endl; // 010

    // printLetters('a', 'z');
    // cout << endl;
    // printLetters('z', 'a');
    // cout << endl;
    // printLetters('z', 'z');
    // cout << endl;

    // printString("abcd");
    // cout << endl;

    // printSeveral('c', 5);
    // cout << endl;
    // printSeveral('c', -5);
    // cout << endl;

    // cout << fact(5) << endl;
    // cout << fact(1) << endl;
    // cout << fact(0) << endl;
    // cout << fact(-5) << endl;
    // cout << factRecursion(5) << endl;
    // cout << factRecursion(1) << endl;
    // cout << factRecursion(0) << endl;
    // cout << factRecursion(-5) << endl;

    // printDigitsVariantA(5);
    // cout << endl;
    // printDigitsVariantA(52);
    // cout << endl;
    // printDigitsVariantA(452);
    // cout << endl;
    // printDigitsVariantA(1092);
    // cout << endl;
    // // printDigits(0);
    // // cout << endl;

    return 0;
}