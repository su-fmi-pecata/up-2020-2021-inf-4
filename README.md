# UP 2020-2021 Inf 4

Task for inf group 4


Bash helper for compile and run \
`run() { g++ -o ${1/.cpp/} ${1} && ./${1/.cpp/} }` \
Usage: `run sample.cpp`

```bash
alias g++11="g++ -std=c++11"
run11() { g++11 -o ${1/.cpp/} ${1} && ./${1/.cpp/} }
```

