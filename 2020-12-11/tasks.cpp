#include <iostream>

using std::cin;
using std::cout;
using std::endl;

// int* modify(const int* arr, const int n) {
//     int* z = new int[n];
//     int r[n]; // no

// const int * a;
// a = z;
// int * const b;
// b = z;
// const int * const c;


//     return z;
// }

// const
// const int *
// int const *
// int* const 
// cont int * const 

int getLength(const char* const str) {
    int len = 0;
    while(str[len]) {
        len++;
    }
    return len;
}

void readString(char* str, int* len, const int* const MAX_LEN) {
    cin.getline(str, *MAX_LEN); // *MAX_LEN == MAX_LEN[0]
    *len = getLength(str);
}

void task1 () {
    const int maxLen = 20;
    char str1[maxLen];
    int result;
    readString(str1, &result, &maxLen);
    cout << "Len is " << result << endl;
}

void printWithoutPointer0(const char* const arr) {
    for(int i = 0;arr[i];i++) {
        if(i % 2 == 0) {
            cout << arr[i];
        }
    }
}
void printWithoutPointer1(const char* const arr) {
    if(arr[0]) {
       cout << arr[0]; 
    } else {
        return;
    }

    for(int i = 2; arr[i - 1] && arr[i]; i = i + 2) {
        cout << arr[i];
    }
}

void printWithoutPointer2(const char* const arr) {
    for(int i = 0; arr[i] && arr[i + 1]; i = i + 2) {
        cout << arr[i];
    }
}


void printWithPointer2a(const char* arr) {
    for(; *arr && *(arr + 1); arr = arr + 2) {
        cout << *arr;
    }
    // arr -> '\0'
}
void printWithPointer2b(const char* const arr) {
    const char* copy = arr;
    for(; *copy && *(copy + 1); copy = copy + 2) {
        cout << *copy;
    }
    // arr -> "This is Sample"
}

void task4() {
    const char arr[] = "This is Sample";
    cout << arr << endl;
    printWithoutPointer2(arr);
    cout << endl;
    printWithPointer2b(arr);
    cout << endl;
}

void redirectPtr(const int** a, const int* const b) {
    *a = b;
}

void task7() {
    int a = 3;
    int b = 5;

    const int* ptra = &a;
    const int* ptrb = &b;

    cout << "Old ptra:" << *ptra << endl;
    redirectPtr(&ptra, ptrb);
    cout << "New ptra:" << *ptra << endl;
    cout << "*ptrb:" << *ptrb << endl;
    cout << "a:" << a << endl;
    cout << "b:" << b << endl;
}

void task3() {
    int a[5];
    for(int i = 0;  i < 5; i++) {
     *(a + i) = 5; // *(a + i) == a[i]
     // int* b;
     // *b == b[0]
    }
}

int main() {
    task7();
    return 0;
}