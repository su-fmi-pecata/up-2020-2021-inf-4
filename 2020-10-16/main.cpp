#include <iostream>

int main() {
    // <type> <name> [= <value>];
    // int counter = 20;
    // while(counter != 0) {
    //     std::cout << counter << " Zdravej" << std::endl;
    //     counter = counter - 1;
    // }

    std::cout << " For starts here" << std::endl;

    for (int counter = 0; counter < 20; counter = counter + 1) {
      std::cout << counter << " Zdravej" << std::endl;
    }

    if(true) {
    int counter = 20;
    }

    return 0;
}